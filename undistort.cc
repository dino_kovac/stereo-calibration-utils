#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {

   if(argc != 3) {
      std::cerr << "Usage: " << argv[0] <<" <image path> <calib params yml>" << std::endl;
      return -1;
   }

   std::string image_file = argv[1];
   std::string calib_params_file = argv[2];

   cv::FileStorage input_params(calib_params_file, CV_STORAGE_READ);
   cv::Mat cameraMatrix, distCoeffs;
   input_params["camera_matrix"] >> cameraMatrix;
   input_params["distortion_coefficients"] >> distCoeffs;
   input_params.release();
   std::cout << cameraMatrix << std::endl << std::endl;

   cv::Mat image = cv::imread(image_file, 0);
   cv::Mat image2 = cv::imread(image_file, 0);

   if(!image.data) {
      std::cerr << "Error loading image!" << std::endl;
      return -1;
   }

   cv::undistort(image, image2, cameraMatrix, distCoeffs);

   cv::imwrite("undistorted.png", image2);

   return 0;
}

#include "opencv2/opencv.hpp"

#include <string>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>

void print_usage(int argc, char** argv) {
   std::cerr << "Usage: " << argv[0] <<" [--reverse] input.yml output.yml" << std::endl;
   exit(-1);
}

/*
 * Converts intrinsic camera params from images in air to params which can be used for the same set of cameras underwater
*/
int main(int argc, char** argv) {

   if (argc != 3 && argc != 4) {
      print_usage(argc, argv);
   }

   std::string input_params_file;
   std::string output_params_file;
   bool reverse;

   if (argc == 3) {
      input_params_file = argv[1];
      output_params_file = argv[2];
      reverse = false;
   } else {
      std::string reverse_option = argv[1];

      if (reverse_option != "--reverse") {
         std::cerr << "Unrecognized option: " << reverse_option << std::endl;
         print_usage(argc, argv);
      }

      reverse = true;
      input_params_file = argv[2];
      output_params_file = argv[3];
   }

   cv::FileStorage input_params(input_params_file, cv::FileStorage::READ);
   if (!input_params.isOpened()) {
      std::cerr << "Error openning input params file" << std::endl;
   }
   cv::FileStorage output_params(output_params_file, cv::FileStorage::WRITE);
   if (!output_params.isOpened()) {
      std::cerr << "Error openning output params file" << std::endl;
   }

   output_params << "image_width" << (int) input_params["image_width"];
   output_params << "image_height" << (int) input_params["image_height"];
   output_params << "board_width" << (int) input_params["board_width"];
   output_params << "board_height" << (int) input_params["board_height"];
   output_params << "square_size" << (double) input_params["square_size"];

   cv::Mat cameraMatrix, distCoeffs;
   input_params["camera_matrix"] >> cameraMatrix;
   input_params["distortion_coefficients"] >> distCoeffs;

   double scaleFactor = reverse ? 0.75 : 1.333;

   cameraMatrix.at<double>(0, 0) *= scaleFactor;
   cameraMatrix.at<double>(1, 1) *= scaleFactor;
   for (int row = 0; row < distCoeffs.rows; ++row) {
      for (int col = 0; col < distCoeffs.cols; ++col) {
         distCoeffs.at<double>(row, col) *= scaleFactor;
      }
   }

   output_params << "camera_matrix" << cameraMatrix;
   output_params << "distortion_coefficients" << distCoeffs;
   output_params << "reprojection_error" << (double) input_params["reprojection_error"];

   input_params.release();
   output_params.release();
   std::cout << cameraMatrix << std::endl << std::endl;
   std::cout << distCoeffs << std::endl << std::endl;

   return 0;
}

INC=`pkg-config --cflags opencv`
OPTIONS=`pkg-config --libs opencv`
CXXFLAGS=-c -O3 -std=c++11 -Wall -Wextra
LDFLAGS=-O3

all: calib_mono calib_stereo imagelist_creator undistort air2water

.cc.o:
	$(CXX) $(CXXFLAGS) $< -o $@ $(INC)

$(EXECUTABLE): $(OBJECTS)
	$(CXX) $(LDFLAGS) $(OBJECTS) -o $@ $(INC) $(OPTIONS)

$(TEST_EXECUTABLE): $(TEST_OBJECTS)
	$(CXX) $(LDFLAGS) $(TEST_OBJECTS) -o $@ $(INC) $(OPTIONS)

imagelist_creator: imagelist_creator.cc calib_helper.h
	$(CXX) $(LDFLAGS) imagelist_creator.cc -o $@ $(INC) $(OPTIONS)

calib_stereo: calibrator_stereo.cc
	$(CXX) $(LDFLAGS) calibrator_stereo.cc -o $@ $(INC) $(OPTIONS)

calib_mono: calibrator_mono.cc calib_helper.h
	$(CXX) $(LDFLAGS) calibrator_mono.cc -o $@ $(INC) $(OPTIONS)

undistort: undistort.cc
	$(CXX) $(LDFLAGS) undistort.cc -o $@ $(INC) $(OPTIONS)

air2water: air2water.cc
	$(CXX) $(LDFLAGS) air2water.cc -o $@ $(INC) $(OPTIONS)

clean:
	-rm calib_mono calib_stereo imagelist_creator undistort

.PHONY: clean
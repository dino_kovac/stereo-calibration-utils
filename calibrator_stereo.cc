// run example:
//./calibrator_stereo_extr -lc bb_left.yml -rc bb_right.yml -w 8 -h 6 -nc -nr -s /home/kreso/projects/master_thesis/datasets/bumblebee/monitor_calib/ ../../../../../datasets/bumblebee/monitor_calib.xml
// ./calib_stereo -lc calib_left.yml -rc calib_right.yml -w 9 -h 9 -nc -nr -s ~/Downloads/calib/skupa/ files.xml
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <cmath>

using namespace cv;
using namespace std;

#define PI 3.14159265

static int print_help()
{
   cout <<
      " Given a list of chessboard images, the number of corners (nx, ny)\n"
      " on the chessboards, and a flag: useCalibrated for \n"
      "   calibrated (0) or\n"
      "   uncalibrated \n"
      "     (1: use cvStereoCalibrate(), 2: compute fundamental\n"
      "         matrix separately) stereo. \n"
      " Calibrate the cameras and display the\n"
      " rectified results along with the computed disparity images.   \n" << endl;
   cout << "Usage:\n ./calibrator -w board_width -h board_height -s src_dir [-nr /*dot not view results*/] <image list XML/YML file>\n" << endl;
   return 0;
}

/**
 * Calculates the anglew which a line defined by two point closes with the X axis
*/
static double calculateAngle(Point2f a, Point2f b) {
   int dx = b.x - a.x;
   int dy = a.y - b.y;
   double angle = atan2(dy, dx) * 180 / PI;
   return angle;
}

static bool angleApproximatelyEqual(double angle1, double angle2) {
   return fabs(angle2 - angle1) < 15.f;
}

static void reverseRows(vector<Point2f> &corners, Size &boardSize) {
   Point2f tmp;
   for(int row = 0; row < boardSize.height / 2; row++) {
      for(int col = 0; col < boardSize.width; col++) {
         tmp = corners[row * boardSize.width + col];
         corners[row * boardSize.width + col] = corners[(boardSize.height - row - 1) * boardSize.width + col];
         corners[(boardSize.height - row - 1) * boardSize.width + col] = tmp;
      }
   }
}

static void reverseCols(vector<Point2f> &corners, Size &boardSize) {
   Point2f tmp;
   for(int row = 0; row < boardSize.height; row++) {
      for(int col = 0; col < boardSize.width / 2; col++) {
         tmp = corners[row * boardSize.width + col];
         corners[row * boardSize.width + col] = corners[row * boardSize.width + (boardSize.width - col - 1)];
         corners[row * boardSize.width + (boardSize.width - col - 1)] = tmp;
      }
   }
}

static void transpose(vector<Point2f> &corners, Size &boardSize) {
   vector<Point2f> tmpCorners;
   for(int row = 0; row < boardSize.height; row++) {
      for(int index = row; index < boardSize.width * boardSize.height; index += boardSize.width) {
         tmpCorners.push_back(corners[index]);
      }
   }
   for (int i = 0; i < corners.size(); ++i) {
      corners[i] = tmpCorners[i];
   }
}

static void stereoCalib(const string& left_cam_file, const string& right_cam_file, const string& src_dir,
                        const vector<string>& imagelist, Size boardSize, bool useCalibrated=true,
                        bool showRectified=true, bool showCorners=true)
{
   // read left and right camera matrix data
   FileStorage input_params(left_cam_file, CV_STORAGE_READ);
   Mat cameraMatrix[2], distCoeffs[2];
   input_params["camera_matrix"] >> cameraMatrix[0];
   input_params["distortion_coefficients"] >> distCoeffs[0];
   input_params.release();
   input_params.open(right_cam_file, CV_STORAGE_READ);
   input_params["camera_matrix"] >> cameraMatrix[1];
   input_params["distortion_coefficients"] >> distCoeffs[1];
   input_params.release();
   cout << cameraMatrix[0] << "\n\n";
   cout << cameraMatrix[1] << "\n\n";

   if( imagelist.size() % 2 != 0 )
   {
      cout << "Error: the image list contains odd (non-even) number of elements\n";
      return;
   }

   const int maxScale = 2;
   //const double squareSize = .12/5.2;   //  TODO Set this to your actual square size
   //const double squareSize = 0.028;       // 28mm on A4 paper
   const double squareSize = 0.04;

   // ARRAY AND VECTOR STORAGE:
   vector<vector<Point2f> > imagePoints[2];
   vector<vector<Point3f> > objectPoints;
   Size imageSize;

   int i, j, k, nimages = (int)imagelist.size()/2;

   imagePoints[0].resize(nimages);
   imagePoints[1].resize(nimages);
   vector<string> goodImageList;

   for(i = j = 0; i < nimages; i++) {

      Point2f leftStartPoint, leftRowPoint, leftColPoint;
      Point2f rightStartPoint, rightRowPoint, rightColPoint;

      for(k = 0; k < 2; k++) {
         const string& filename = src_dir + imagelist[i*2+k];
         Mat img = imread(filename, 0);
         if(img.empty())
            break;
         if( imageSize == Size() )
            imageSize = img.size();
         else if( img.size() != imageSize )
         {
            cout << "The image " << filename << " has the size different from the first image size. Skipping the pair\n";
            break;
         }
         bool found = false;
         vector<Point2f>& corners = imagePoints[k][j];
         for( int scale = 1; scale <= maxScale; scale++ )
         {
            Mat timg;
            if( scale == 1 )
               timg = img;
            else
               resize(img, timg, Size(), scale, scale);
            found = findChessboardCorners(timg, boardSize, corners, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_NORMALIZE_IMAGE);
            // cout << "corners = " << corners << endl;

            if(found)
            {
               if(k == 0) {
                  // first image
                  leftStartPoint = corners[0];
                  leftRowPoint = corners[1];
                  leftColPoint = corners[boardSize.width];
               } else if (k == 1) {
                  // second image
                  rightStartPoint = corners[0];
                  rightRowPoint = corners[1];
                  rightColPoint = corners[boardSize.width];
                  double leftRowAngle = calculateAngle(leftStartPoint, leftRowPoint);
                  double leftColAngle = calculateAngle(leftStartPoint, leftColPoint);
                  double rightRowAngle = calculateAngle(rightStartPoint, rightRowPoint);
                  double rightColAngle = calculateAngle(rightStartPoint, rightColPoint);

                  double rowAngleDiff = leftRowAngle - rightRowAngle;
                  double colAngleDiff = leftColAngle - rightColAngle;

                  cout << "left row: " << leftRowAngle << " left col: " << leftColAngle << endl;
                  cout << "right row: " << rightRowAngle << " right col: " << rightColAngle << endl;

                  if (angleApproximatelyEqual(rowAngleDiff, 0) && angleApproximatelyEqual(colAngleDiff, 0)) {
                     // case 1 - orientations are fine, nothing to do here!
                     /*
                     *  Graphical representation: (@ represents the position of the first corner)
                     *  ROW represents the direction of the row vector, COL represents the direction of the column vector
                     *
                     *  left        right
                     *  @R-O-W->    @R-O-W->
                     *  C           C
                     *  O           O
                     *  L           L
                     *  v           v
                     */
                  } else if(angleApproximatelyEqual(rowAngleDiff, 90) && angleApproximatelyEqual(colAngleDiff, -90)) {
                     /*
                     *  left        right
                     *  @R-O-W->    @C-O-L->
                     *  C           R
                     *  O           O
                     *  L           W
                     *  v           v
                     */
                     cout << "case 2 - TRANSPOSE" << endl;
                     transpose(corners, boardSize);
                  } else if(angleApproximatelyEqual(fabs(rowAngleDiff), 180) && angleApproximatelyEqual(colAngleDiff, 0)) {
                     /*
                     *  Graphical representation: (@ represents the position of the first corner)
                     *  left        right
                     *  @R-O-W->    <-W-O-R@
                     *  C                  C
                     *  O                  O
                     *  L                  L
                     *  v                  v
                     */
                     cout << "case 3 - REVERSE COLS" << endl;
                     reverseCols(corners, boardSize);
                  } else if(angleApproximatelyEqual(rowAngleDiff, 90) && angleApproximatelyEqual(colAngleDiff, 90)) {
                     /*
                     *  left        right
                     *  @R-O-W->    <-L-O-C@
                     *  C                  R
                     *  O                  O
                     *  L                  W
                     *  v                  v
                     */
                     cout << "case 4 - REVERSE ROWS + TRANSPOSE" << endl;
                     reverseRows(corners, boardSize);
                     transpose(corners, boardSize);
                  } else if(angleApproximatelyEqual(rowAngleDiff, 0) && angleApproximatelyEqual(fabs(colAngleDiff), 180)) {
                     /*
                     *  left        right
                     *  @R-O-W->    ^
                     *  C           L
                     *  O           O
                     *  L           C
                     *  v           @R-O-W->
                     */
                     cout << "case 5 - REVERSE ROWS" << endl;
                     reverseRows(corners, boardSize);
                  } else if(angleApproximatelyEqual(rowAngleDiff, -90) && angleApproximatelyEqual(colAngleDiff, -90)) {
                     /*
                     *  left        right
                     *  @R-O-W->    ^
                     *  C           W
                     *  O           O
                     *  L           R
                     *  v           @C-O-L->
                     */
                     cout << "case 6 - REVERSE COLS + TRANSPOSE" << endl;
                     reverseCols(corners, boardSize);
                     transpose(corners, boardSize);
                  } else if(angleApproximatelyEqual(fabs(rowAngleDiff), 180) && angleApproximatelyEqual(fabs(colAngleDiff), 180)) {
                     /*
                     *  left        right
                     *  @R-O-W->          ^
                     *  C                 L
                     *  O                 O
                     *  L                 C
                     *  v          <-W-O-R@
                     */
                     cout << "case 7 - REVERSE COLS + REVERSE ROWS" << endl;
                     reverseCols(corners, boardSize);
                     reverseRows(corners, boardSize);
                  } else if(angleApproximatelyEqual(rowAngleDiff, -90) && angleApproximatelyEqual(colAngleDiff, 90)) {
                     /*
                     *  left        right
                     *  @R-O-W->          ^
                     *  C                 W
                     *  O                 O
                     *  L                 R
                     *  v          <-L-O-C@
                     */
                     cout << "case 8 - REVERSE COLS + REVERSE ROWS + TRANSPOSE" << endl;
                     reverseCols(corners, boardSize);
                     reverseRows(corners, boardSize);
                     transpose(corners, boardSize);
                  } else {
                     cerr << "Angles don't match a known case!" << endl;
                  }
               }
               if( scale > 1 )
               {
                  Mat cornersMat(corners);
                  cornersMat *= 1./scale;
               }
               break;
            }
         }
         if(showCorners)
         {
            cout << filename << endl;
            Mat cimg, cimg1;
            cvtColor(img, cimg, CV_GRAY2BGR);
            drawChessboardCorners(cimg, boardSize, corners, found);
            double sf = 640./MAX(img.rows, img.cols);
            resize(cimg, cimg1, Size(), sf, sf);
            imshow("corners" + to_string(k), cimg1);
            char c = (char)waitKey(500);
            if( c == 27 || c == 'q' || c == 'Q' ) //Allow ESC to quit
               exit(-1);
         }
         else
            putchar('.');
         cout << "found: " << found << endl;
         if(!found)
            break;
         cornerSubPix(img, corners, Size(11,11), Size(-1,-1), TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 30, 0.01));
      }
      waitKey(0);

      if( k == 2 )
      {
         goodImageList.push_back(imagelist[i*2]);
         goodImageList.push_back(imagelist[i*2+1]);
         j++;
      }
   }
   cout << j << " pairs have been successfully detected.\n";
   nimages = j;
   if( nimages < 2 )
   {
      cout << "Error: too little pairs to run the calibration\n";
      return;
   }

   imagePoints[0].resize(nimages);
   imagePoints[1].resize(nimages);
   objectPoints.resize(nimages);

   for( i = 0; i < nimages; i++ )
   {
      for( j = 0; j < boardSize.height; j++ )
         for( k = 0; k < boardSize.width; k++ )
            objectPoints[i].push_back(Point3f(j*squareSize, k*squareSize, 0));
   }

   cout << "Running stereo calibration ...\n";


   Mat R, T, E, F;

   double rms = stereoCalibrate(objectPoints, imagePoints[0], imagePoints[1],
         cameraMatrix[0], distCoeffs[0], cameraMatrix[1], distCoeffs[1],
         imageSize, R, T, E, F, TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 100, 1e-5),
         CV_CALIB_FIX_INTRINSIC);
   cout << "done with RMS error=" << rms << endl;

   cout << "R: " << R << endl;
   cout << "T: " << T << endl;

   // CALIBRATION QUALITY CHECK
   // because the output fundamental matrix implicitly
   // includes all the output information,
   // we can check the quality of calibration using the
   // epipolar geometry constraint: m2^t*F*m1=0
   double err = 0;
   int npoints = 0;
   vector<Vec3f> lines[2];
   for( i = 0; i < nimages; i++ )
   {
      int npt = (int)imagePoints[0][i].size();
      Mat imgpt[2];
      for( k = 0; k < 2; k++ )
      {
         imgpt[k] = Mat(imagePoints[k][i]);
         undistortPoints(imgpt[k], imgpt[k], cameraMatrix[k], distCoeffs[k], Mat(), cameraMatrix[k]);
         computeCorrespondEpilines(imgpt[k], k+1, F, lines[k]);
      }
      for( j = 0; j < npt; j++ )
      {
         double errij = fabs(imagePoints[0][i][j].x*lines[1][j][0] +
               imagePoints[0][i][j].y*lines[1][j][1] + lines[1][j][2]) +
            fabs(imagePoints[1][i][j].x*lines[0][j][0] +
                  imagePoints[1][i][j].y*lines[0][j][1] + lines[0][j][2]);
         err += errij;
      }
      npoints += npt;
   }
   cout << "average reprojection err = " <<  err/npoints << endl;

   // save intrinsic parameters
   FileStorage fs("calib_params.yml", CV_STORAGE_WRITE);
   if( fs.isOpened() )
   {
      fs << "M1" << cameraMatrix[0] << "D1" << distCoeffs[0] << "M2" << cameraMatrix[1]
         << "D2" << distCoeffs[1] << "R" << R << "T" << T << "E" << E << "F" << F;
      fs.release();
   }
   else
      cout << "Error: can not save the intrinsic parameters\n";

   Mat R1, R2, P1, P2, Q;
   Rect validRoi[2];

   double alpha = 0.0; // 0 to zoom to ROI, 1 not to crop...
   stereoRectify(cameraMatrix[0], distCoeffs[0],
         cameraMatrix[1], distCoeffs[1],
         imageSize, R, T, R1, R2, P1, P2, Q,
         CALIB_ZERO_DISPARITY, alpha, imageSize, &validRoi[0], &validRoi[1]);

   fs.open("calib_rectif_params.yml", CV_STORAGE_WRITE);
   if( fs.isOpened() )
   {
      fs << "R1" << R1 << "R2" << R2 << "P1" << P1 << "P2" << P2 << "Q" << Q;
      fs.release();
   }
   else
      cout << "Error: can not save the extrinsic parameters\n";

   // OpenCV can handle left-right
   // or up-down camera arrangements
   bool isVerticalStereo = fabs(P2.at<double>(1, 3)) > fabs(P2.at<double>(0, 3));

   // COMPUTE AND DISPLAY RECTIFICATION - only for visual demonstration
   if( !showRectified )
      return;

   Mat rmap[2][2];
   // IF BY CALIBRATED (BOUGUET'S METHOD)
//   if( useCalibrated )
//   {
//      // we already computed everything
//   }
//   // OR ELSE HARTLEY'S METHOD - just for uncalibrated cameras
//   else
//      // use intrinsic parameters of each camera, but
//      // compute the rectification transformation directly
//      // from the fundamental matrix
//   {
//      vector<Point2f> allimgpt[2];
//      for( k = 0; k < 2; k++ )
//      {
//         for( i = 0; i < nimages; i++ )
//            std::copy(imagePoints[k][i].begin(), imagePoints[k][i].end(), back_inserter(allimgpt[k]));
//      }
//      F = findFundamentalMat(Mat(allimgpt[0]), Mat(allimgpt[1]), FM_8POINT, 0, 0);
//      Mat H1, H2;
//      stereoRectifyUncalibrated(Mat(allimgpt[0]), Mat(allimgpt[1]), F, imageSize, H1, H2, 3);
//
//      R1 = cameraMatrix[0].inv()*H1*cameraMatrix[0];
//      R2 = cameraMatrix[1].inv()*H2*cameraMatrix[1];
//      P1 = cameraMatrix[0];
//      P2 = cameraMatrix[1];
//   }

   //Precompute maps for cv::remap()
   initUndistortRectifyMap(cameraMatrix[0], distCoeffs[0], R1, P1, imageSize, CV_16SC2, rmap[0][0], rmap[0][1]);
   initUndistortRectifyMap(cameraMatrix[1], distCoeffs[1], R2, P2, imageSize, CV_16SC2, rmap[1][0], rmap[1][1]);

   Mat canvas;
   double sf;
   int w, h;
   if( !isVerticalStereo )
   {
      sf = 600./MAX(imageSize.width, imageSize.height);
      w = cvRound(imageSize.width*sf);
      h = cvRound(imageSize.height*sf);
      canvas.create(h, w*2, CV_8UC3);
   }
   else
   {
      sf = 300./MAX(imageSize.width, imageSize.height);
      w = cvRound(imageSize.width*sf);
      h = cvRound(imageSize.height*sf);
      canvas.create(h*2, w, CV_8UC3);
   }

   for(i = 0; i < nimages; i++)
   {
      for(k = 0; k < 2; k++)
      {
         Mat img = imread(src_dir + goodImageList[i*2+k], 0), rimg, cimg;
         remap(img, rimg, rmap[k][0], rmap[k][1], CV_INTER_LINEAR);
         cvtColor(rimg, cimg, CV_GRAY2BGR);
         Mat canvasPart = !isVerticalStereo ? canvas(Rect(w*k, 0, w, h)) : canvas(Rect(0, h*k, w, h));
         resize(cimg, canvasPart, canvasPart.size(), 0, 0, CV_INTER_AREA);
         if( useCalibrated )
         {
            Rect vroi(cvRound(validRoi[k].x*sf), cvRound(validRoi[k].y*sf),
                  cvRound(validRoi[k].width*sf), cvRound(validRoi[k].height*sf));
            rectangle(canvasPart, vroi, Scalar(0,0,255), 3, 8);
            // TODO
         }
      }

      if( !isVerticalStereo )
         for( j = 0; j < canvas.rows; j += 16 )
            line(canvas, Point(0, j), Point(canvas.cols, j), Scalar(0, 255, 0), 1, 8);
      else
         for( j = 0; j < canvas.cols; j += 16 )
            line(canvas, Point(j, 0), Point(j, canvas.rows), Scalar(0, 255, 0), 1, 8);
      imshow("rectified", canvas);
      // for demo imwrite("rectif_demo.png", canvas);
      char c = (char)waitKey();
      if( c == 27 || c == 'q' || c == 'Q' )
         break;
   }
}


static bool readStringList( const string& filename, vector<string>& l )
{
   l.resize(0);
   FileStorage fs(filename, FileStorage::READ);
   if( !fs.isOpened() )
      return false;
   FileNode n = fs.getFirstTopLevelNode();
   if( n.type() != FileNode::SEQ )
      return false;
   FileNodeIterator it = n.begin(), it_end = n.end();
   for( ; it != it_end; ++it )
      l.push_back((string)*it);
   return true;
}

int main(int argc, char** argv)
{
   Size boardSize;
   string src_dir;
   string imagelistfn;
   string left_cam_file, right_cam_file;
   bool showRectified = true;
   bool showCorners = true;

   for( int i = 1; i < argc; i++ )
   {
      if( string(argv[i]) == "-w" )
      {
         if( sscanf(argv[++i], "%d", &boardSize.width) != 1 || boardSize.width <= 0 )
         {
            cout << "invalid board width" << endl;
            return print_help();
         }
      }
      else if( string(argv[i]) == "-h" )
      {
         if( sscanf(argv[++i], "%d", &boardSize.height) != 1 || boardSize.height <= 0 )
         {
            cout << "invalid board height" << endl;
            return print_help();
         }
      }
      else if( string(argv[i]) == "-lc" )
         left_cam_file = argv[++i];
      else if( string(argv[i]) == "-rc" )
         right_cam_file = argv[++i];
      else if( string(argv[i]) == "-nr" )
         showRectified = false;
      else if(string(argv[i]) == "-nc")
         showCorners = false;
      else if( string(argv[i]) == "--help" )
         return print_help();
      else if(string(argv[i]) == "-s")
         src_dir = argv[++i];
      else if( argv[i][0] == '-' )
      {
         cout << "invalid option " << argv[i] << endl;
         return 0;
      }
      else
         imagelistfn = argv[i];
   }

   if(imagelistfn == "")
   {
      cerr << "no xml file list argument\n";
      return -1;
   }
   else if( boardSize.width <= 0 || boardSize.height <= 0 )
   {
      cout << "error: if you specified XML file with chessboards, you should also specify the board width and height (-w and -h options)" << endl;
      return 0;
   }

   vector<string> imagelist;
   bool ok = readStringList(imagelistfn, imagelist);
   if(!ok || imagelist.empty())
   {
      cout << "can not open " << imagelistfn << " or the string list is empty" << endl;
      return print_help();
   }

   stereoCalib(left_cam_file, right_cam_file, src_dir, imagelist, boardSize, true, showRectified, showCorners);
   return 0;
}
